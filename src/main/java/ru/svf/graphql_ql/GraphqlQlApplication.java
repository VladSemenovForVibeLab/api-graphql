package ru.svf.graphql_ql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlQlApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlQlApplication.class, args);
	}

}

# Учет продавцов и адресов, контактов на Spring Boot

Это приложение предназначено для учета продавцов, используя информацию адресов и контактов. Оно разработано на базе Spring Boot, использует GraphQL для обработки запросов и хранит данные в базе данных H2.

## Установка

1. Установите Java Development Kit (JDK) версии 17.
2. Склонируйте репозиторий на свой локальный компьютер.
3. Перейдите в корневую директорию проекта.
4. Откройте командную строку или терминал и выполните следующую команду для запуска приложения:

```
./mvnw spring-boot:run
```

## Зависимости

Приложение использует следующие зависимости:

- Spring Boot Starter Web для создания веб-приложения.
- GraphQL Java для обработки запросов GraphQL.
- GraphQL Scalars для поддержки скалярных типов.
- Springdoc OpenAPI для генерации документации API.
- H2 Database для хранения данных в памяти.
- Spring Boot Configuration Processor для обработки конфигурации.
- Lombok для автоматической генерации геттеров, сеттеров и других методов.
- Spring WebFlux для реактивного программирования.
- Spring Data JPA для работы с базой данных.

## Описание модели данных

### Merchant

Класс Merchant представляет собой модель продавца.

Поля:

- id - уникальный идентификатор продавца.
- name - имя продавца.
- surname - фамилия продавца.
- taxNumber
- identityNumber - уникальный идентификатор
- companyType - тип компании -> организации
Связи:

- address - объект класса Address, содержащий информацию об адресе продавца.
- contacts - список объектов класса Contact, содержащих контактные данные продавца.

### Address

Класс Address представляет собой модель адреса.

Поля:

- id - уникальный идентификатор адреса.
- street - название улицы.
- city - название города.
- country - название страны.
- zipCode - код

Связи:

- merchant  - объект класса Merchant, содержащий информацию о продавцe.

### Contact

Класс Contact представляет собой модель контактных данных.

Поля:

- id - уникальный идентификатор контакта.
- name - имя контакта.
- surname - фамилия контакта.
- email - адрес электронной почты.
- phone - номер телефона.

Связи:

- merchant  - объект класса Merchant, содержащий информацию о продавцe.

## GraphQL API

Приложение предоставляет следующий GraphQL API:

### Запросы (Queries)

- getMerchants - получить список всех продавцов.

```
query{
    getMerchants{
        id
        name
        surname
        taxNumber
        address{
            city
           }
        contacts{
        name
        surname
        phone
        email
        }
    }
}
```

```
query{
  getMerchantById(id: 2){
    id
    name
    identityNumber
    surname
    address{
      city
      zipCode
      street
      country
    }
  }
}
```

### Мутации (Mutations)

- createMerchant - создать нового продавца.

```
mutation {
  createMerchant(merchant:{
    name: "Vlad",
    surname:" Semenov",
    taxNumber: "11111111",
    identityNumber:"111",
    companyType: INDIVIDUAL,
    address: {
      city: "SPB",
      country: "RU",
      street:" PUSH",
      zipCode: "456734"
    },
    contacts: [
      {
        name: "Vlad",
      	surname:"Semenov",
        email:"ooovladislavchik@gmail.com",
        phone: "89184842889"
      },
       {
        name: "Tolyk",
      	surname:"Semenov",
        email:"toly@gmail.com",
        phone: "7773838776"
      }
    ]
  }){
    id
    name
    surname
    contacts{
      name
      phone
    }
  }
}
```

## Документация API

Документация API доступна по адресу http://localhost:8080/swagger-ui.html после запуска приложения.
package ru.svf.graphql_ql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.svf.graphql_ql.model.Contact;
@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
}
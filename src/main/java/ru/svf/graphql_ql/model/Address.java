package ru.svf.graphql_ql.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "addresses")
public class Address {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "STREET", nullable = false, length = 50)
    private String street;

    @Column(name = "CITY", nullable = false, length = 50)
    private String city;

    @Column(name = "COUNTRY", nullable = false, length = 50)
    private String country;

    @Column(name = "ZIP_CODE", nullable = false, length = 10)
    private String zipCode;

    @OneToOne(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Merchant merchant;
}


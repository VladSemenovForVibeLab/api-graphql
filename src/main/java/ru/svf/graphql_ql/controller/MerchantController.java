package ru.svf.graphql_ql.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import ru.svf.graphql_ql.model.Address;
import ru.svf.graphql_ql.model.Contact;
import ru.svf.graphql_ql.model.Merchant;
import ru.svf.graphql_ql.repository.MerchantRepository;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class MerchantController {
    private final MerchantRepository merchantRepository;

    @MutationMapping
    public Merchant createMerchant(@Argument("merchant") Merchant merchant) {
        return merchantRepository.save(merchant);
    }

    @QueryMapping
    public List<Merchant> getMerchants() {
        return merchantRepository.findAll();
    }

    @QueryMapping
    public Merchant getMerchantById(@Argument("id") Integer id) {
        return merchantRepository.findById(id).orElseThrow(()->new RuntimeException("Merchant not found"));
    }

    @SchemaMapping
    public List<Contact> contacts(Merchant merchant){
        return merchant.getContacts();
    }

    @SchemaMapping
    public Address address(Merchant merchant) {
        return merchant.getAddress();
    }
}

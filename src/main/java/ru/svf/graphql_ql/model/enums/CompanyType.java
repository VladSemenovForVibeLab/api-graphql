package ru.svf.graphql_ql.model.enums;

public enum CompanyType {
    INDIVIDUAL, COOPERATIVE
}
